import imutils
import cv2

image = cv2.imread("immagine-mare.jpg")
image2 = cv2.imread("pencil-image.jpg")

(h, w, d) = image.shape
print("width={}, height={}, depth={}".format(w, h, d))

rotated = imutils.rotate_bound(image, 90)
cv2.imshow("rotated", rotated)

partOfImage = image[50:200, 320:420]
cv2.imshow("parte di immagine", partOfImage)

scale = 300 / w
newSize = (300, int(h * scale))
resized = cv2.resize(image, newSize)
cv2.imshow("resized", resized)

cv2.line(image, (120,120), (250,250), (0,0,255), 2)

#center = (w, h/2, d)
cv2.circle(image, (200, 350), 50, (0,255,0), 2)

cv2.rectangle(image, (250,50), (350, 150), (255,0,0), 4)

cv2.putText(image, "Testo scritto con opencv", (600,600), cv2.FONT_HERSHEY_PLAIN, 1, (255,0,0), 1)

sfumatura = cv2.GaussianBlur(image2, (13,13), 1)
cv2.imshow("Sfumatura", sfumatura)

bordo = cv2.Canny(image2, 50, 150)
cv2.imshow("Canny", bordo)

thresh = cv2.threshold(image2, 225, 255, cv2.THRESH_BINARY_INV ) [1]  #errore TypeError: 'int' object is not subscriptable
cv2.imshow("Thresholding", thresh)

copy = thresh.copy()
copy = cv2.erode(copy, None, iterations=7)
cv2.imshow("eroded", copy)

copy = cv2.dilate(copy, None, iterations=5)
cv2.imshow("dilated", copy)

cv2.imshow("mare", image)
cv2.imshow("pencil", image2)
cv2.waitKey(0)